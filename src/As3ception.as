package gpumovieclip.libs {
    import flash.events.Event;
    import flash.utils.SetIntervalTimer;

    /**
     * ...
     * @author mosowski
     */
    public final class As3ception {
        private var src:String;
        private var tp:int;
        private var pp:int;
        private var tokens:Vector.<Token> = new Vector.<Token>();

        private var env:Environment;
        private var exp:Expression;

        private static var opPrio:Array = ["!", "neg", "--", "++", "!= ", "==",">=", "<=", ">", "<", "&&", "||", "%", "*", "/", "+", "-", "=" ];
        private static var opUnary:Array  = ["!", "neg", "--", "++" ];
        private static var expEnds:Array = [";", ")", "]", ",", ":", "}"];
        private static var opDblChar:Array = ["++", "--", "==", "!=", ">=", "<=", "&&", "||"];
        private static var opClass:Array;

        public function As3ception(source:String, environment:Object = null) {
            if (!opClass) {
                opClass = [ ENot, ENeg, EDec, EInc, ENotEqual, EEqual, EGEqual, ELEqual, EGreater, ELess, EAnd, EOr, EMod, EMul, EDiv, EAdd, ESub, EAssign ];
            }
            src = source;
            tokenize();


            exp = parseStatement();
            if (environment) {
                env = environment as Environment;
            }
            if (!env) {
                env = new Environment();
                env.root = new Object();
                env.addDefaultScopeObject(env.root);
            }

            tokens = null;
            pp = 0;
            tp = 0;
        }

        public function getEnv():Environment {
            return env;
        }

        public function setEnv(env:Object):void {
            this.env = env as Environment;
        }

        public static function newEnv():Object {
            var env:Environment = new Environment();
            env.root = new Object();
            env.addDefaultScopeObject(env.root);
            return env;
        }

        public function eval():Object {
            env.initDefaultScope();
            return exp.getValue(env);
        }

        private function tokenize():void {
            tp = 0;
            skipWhitespace();
            while (tp < src.length) {
                tokens.push(getNextToken());
                skipWhitespace();
            }
        }

        private function getNextToken():Token {
            skipWhitespace();
            var t:Token;
            if (tp < src.length) {
                if ( (isDigit(src.charAt(tp)))
                    || (tp+1 < src.length && src.charAt(tp) == "." && isDigit(src.charAt(tp+1))) ) {
                    t = new TokenNumber();
                    t.valuenum = parseNumber();
                } else if (isAlpha(src.charAt(tp))) {
                    t = new TokenWord();
                    t.valuestr = parseWord();
                } else if (src.charAt(tp) == '"') {
                    t =  new TokenString();
                    t.valuestr = parseString();
                } else {
                    t = new TokenSymbol();
                    t.valuestr = parseSymbol();
                }
            }
            return t;
        }

        private function skipWhitespace():void {
            var char:String;
            while (tp < src.length && isWhitespace(src.charAt(tp))) {
                tp++;
            }
            if (tp + 1 < src.length) {
                if (src.substr(tp, 2) == "//") {
                    skipLineComment();
                } else if (src.substr(tp, 2) == "/*") {
                    skipBlockComment();
                }
            }
        }

        private function isWhitespace(s:String):Boolean {
            return s == " " || s == "\n" || s == "\t" || s == "\r";
        }

        private function isDigit(s:String):Boolean {
            return s.charCodeAt(0) >= "0".charCodeAt(0) && s.charCodeAt(0) <= "9".charCodeAt(0);
        }

        private function isAlpha(s:String):Boolean {
            return s.charCodeAt(0) >= "a".charCodeAt(0) && s.charCodeAt(0) <= "z".charCodeAt(0)
                || s.charCodeAt(0) >= "A".charCodeAt(0) && s.charCodeAt(0) <= "Z".charCodeAt(0)
                || s.charAt(0) == "_";
        }

        private function parseNumber():Number {
            var n:String = "";
            var dc:int = 0;
            while (tp < src.length && (isDigit(src.charAt(tp)) || (src.charAt(tp) == "." && dc == 0 && ++dc == 1))) {
                n += src.charAt(tp++);
            }
            if (isAlpha(src.charAt(tp))) {
                throw "Bad syntax";
            }
            return parseFloat(n);
        }

        private function parseWord():String {
            var w:String = "";
            while (isAlpha(src.charAt(tp)) || isDigit(src.charAt(tp))) {
                w += src.charAt(tp++);
            }
            return w;
        }

        private function parseString():String {
            var s:String = "";
            var escaped:Boolean = false;
            tp++;
            while (src.charAt(tp) != '"' && !escaped) {
                escaped = false;
                if (src.charAt(tp) == '\\') {
                    escaped=true;
                }
                s += src.charAt(tp++);
            }
            tp++;
            return s;
        }

        private function parseSymbol():String {
            if (tp + 1 < src.length) {
                var op:String = src.substr(tp, 2);
                if (opDblChar.indexOf(op) != -1) {
                    tp += 2;
                    return op;
                }
            }
            return src.charAt(tp++);
        }

        private function skipLineComment():void {
            while (tp < src.length && src.charAt(tp) != '\n') {
                tp++;
            }
            tp++;
            skipWhitespace();
        }

        private function skipBlockComment():void {
            while (tp + 1 < src.length && (src.charAt(tp) != '*' || src.charAt(tp + 1) != '/')) {
                tp++;
            }
            tp += 2;
            skipWhitespace();
        }



        private function getTokensAround(pos:int, radius:int = 5):String {
            return tokens.slice(Math.max(0, pos - radius), Math.max(0, pos - 1)).join(" ") + ">>>" + tokens[pos].toString()
                + "<<<" +tokens.slice(Math.min(tokens.length -1, pos+1, Math.min(tokens.length - 1, pos + radius))).join(" ");
        }

        //---------------------------------------------------------------------
        private function parseToken(token:String, throwAtMiss:Boolean = true):Boolean {
            if (tokens[pp].valuestr != token) {
                if (throwAtMiss) {
                    throw "Missing token '" + token + "'.";
                } else {
                    return false;
                }
            } else {
                pp++;
                return true;
            }
        }

        private function parseBlock():Statement {
            pp++; // {
            var s:Statement = parseStatement();
            parseToken("}");
            return s;
        }

        private function parseStatement():Statement {
            var s:Statement = new Statement();
            s.expression = parseExpression();
            if (pp < tokens.length) {
                if (tokens[pp].valuestr == "}") {
                    return s;
                } else {
                    if ((tokens[pp].valuestr == ";" && pp++) || (tokens[pp - 1].valuestr == "}")) {
                        if (pp < tokens.length) {
                            if (tokens[pp].valuestr != "case" && tokens[pp].valuestr != "default") {
                                s.nextStatement = parseStatement();
                            }
                        }
                    }

                    return s;
                }
            } else {
                return s;
            }
        }

        private function parseExpression():Expression {
            if (pp >= tokens.length) {
                return null;
            }

            if (tokens[pp].valuestr == "var") {
                return parseVarDecl();
            } else if (tokens[pp].valuestr == "{") {
                return parseBlock();
            } else if (tokens[pp].valuestr == "if") {
                return parseIf();
            } else if (tokens[pp].valuestr == "while") {
                return parseWhile();
            } else if (tokens[pp].valuestr == "for") {
                return parseFor();
            } else if (tokens[pp].valuestr == "function") {
                return parseFunction();
            } else if (tokens[pp].valuestr == "return") {
                return parseReturn();
            } else if (tokens[pp].valuestr == "new") {
                return parseNew();
            } else if (tokens[pp].valuestr == "switch") {
                return parseSwitch();
            } else if (tokens[pp].valuestr == "break") {
                pp++;
                return new EBreak();
            } else if (tokens[pp].valuestr == "import") {
                return parseImport();
            }
            else {
                return parseExpressionArith();
            }
        }

        private function parseVarDecl():EVarDecl {
            var e:EVarDecl = new EVarDecl();
            pp++; // var
            e.varName = tokens[pp++].valuestr;
            if (tokens[pp].valuestr == ":") {
                pp += 2;

            }
            if (tokens[pp].valuestr == ";") {
                return e;
            } else if (tokens[pp].valuestr == "=") {
                pp++;
                e.initExp = parseExpression();
            }

            if (tokens[pp].valuestr == ",") {
                pp++;
                e.nextEVarDecl = parseVarDecl();
            }
            return e;
        }

        private function parseFunction():Expression {
            pp++; // function kw
            if (tokens[pp].valuestr == "(") {
                var ef:EFunction = new EFunction();
                ef.args = parseFunctionArgs();
                if (tokens[pp].valuestr == ":") {
                    pp += 2;
                }
                ef.body = parseBlock();
                return ef;
            } else { // function declaration
                var vd:EVarDecl = new EVarDecl();
                vd.varName = tokens[pp++].valuestr;

                ef = new EFunction();
                ef.args = parseFunctionArgs();
                if (tokens[pp].valuestr == ":") { // skip fction type
                    pp += 2;
                }
                ef.body = parseBlock();

                vd.initExp = ef;
                return vd;
            }
        }

        private function parseFunctionArgs():Vector.<String> {
            var args:Vector.<String> = new Vector.<String>();
            pp++; // (
            while (tokens[pp].valuestr != ")") {
                args.push(tokens[pp++].valuestr);
                if ( tokens[pp].valuestr == ":") {
                    pp += 2; // skip type
                }
                if (tokens[pp].valuestr == ",") {
                    pp++;
                }
            }
            pp++;
            return args;
        }

        private function parseReturn():Expression {
            pp++; // return
            var er:EReturn = new EReturn();
            er.e = parseExpression();
            return er;
        }

        private function parseNew():Expression {
            pp++; // new
            var en:ENew = new ENew();
            en.className = tokens[pp++].valuestr;
            if (tokens[pp].valuestr == "(") {
                parseArgsList(en.args);
            }
            return en;
        }

        private function parseArgsList(args:Vector.<Expression> = null):Vector.<Expression> {
            if (!args) {
                args = new Vector.<Expression>();
            }
            pp++; // skip (
            while (pp < tokens.length) {
                if (tokens[pp].valuestr == ")") {
                    pp++;
                    break;
                } else if (tokens[pp].valuestr == ",") {
                    pp++;
                }
                args.push(parseExpression());
            }
            return args;
        }



        private function parseIf():EIf {
            var e:EIf = new EIf();
            pp++; // if
            parseToken("(");
            e.cond = parseExpression();
            parseToken(")");
            e.ifTrue = parseExpression();

            if (pp < tokens.length && tokens[pp].valuestr == "else") {
                pp++;
                e.ifFalse = parseExpression();
            }
            return e;
        }

        private function parseWhile():EWhile {
            var e:EWhile = new EWhile();
            pp++; // while
            parseToken("(");
            e.cond = parseExpression();
            parseToken(")");
            e.body  = parseExpression();
            return e;
        }

        private function parseFor():EFor {
            var e:EFor = new EFor();
            pp++;
            parseToken("(");
            e.init = parseExpression();
            parseToken(";");
            e.cond = parseExpression();
            parseToken(";");
            e.increment = parseExpression();
            parseToken(")");
            e.body = parseExpression();
            return e;
        }

        private function parseSwitch():ESwitch {
            var es:ESwitch = new ESwitch();
            pp++; // switch
            parseToken("(");
            es.value = parseExpression();
            parseToken(")");
            parseToken("{");
            while (pp < tokens.length) {
                if (tokens[pp].valuestr == "case") {
                    pp++;
                    var sb:SwitchBranch = new SwitchBranch();
                    sb.value = parseExpression();
                    parseToken(":");
                    sb.action = parseStatement();
                    es.cases.push(sb);
                } else if (tokens[pp].valuestr == "default") {
                    pp++;
                    parseToken(":");
                    es.caseDefault = parseStatement();
                } else if(tokens[pp].valuestr == "}") {
                    pp++;
                    break;
                } else {
                    throw "Unknown token after case here: " + getTokensAround(pp);
                }
            }
            return es;
        }

        private function parseImport():Expression {
            var ei:EImport = new EImport();
            pp++; // import
            parseValue();
            return ei;
        }

        private function parseExpressionArith_rollStack(opStack:Vector.<String>, valStack:Vector.<Expression>, prio:int):void {
            while (opStack.length && opPrio.indexOf(opStack[opStack.length-1]) <= prio) {
                var op:String = opStack.pop();
                var exp:EArith = new opClass[opPrio.indexOf(op)]();
                exp.right = valStack.pop();
                exp.left = valStack.pop();
                valStack.push(exp);
            }
        }

        private function parseExpressionArith():Expression {
            var opStack:Vector.<String> = new Vector.<String>();
            var valStack:Vector.<Expression> = new Vector.<Expression>();
            var lastVal:Boolean = false;

            while (pp < tokens.length) {
                if (tokens[pp].valuestr == "(") {
                    pp++;
                    valStack.push(parseExpression());
                    parseToken(")");
                    lastVal = true;
                } else {
                    // detect unary minus preceeding value
                    if (!lastVal && tokens[pp].valuestr == "-") {
                        tokens[pp].valuestr = "neg";
                    }

                    var prio:int = opPrio.indexOf(tokens[pp].valuestr);
                    if (prio == -1) {
                        if (expEnds.indexOf(tokens[pp].valuestr) != -1) {
                            break;
                        } else {
                            valStack.push(parseValue());

                            // roll waiting unary prefix operators if any
                            if (opStack.length && opUnary.indexOf(opStack[opStack.length - 1]) != -1) {
                                var e:EArith = new opClass[opPrio.indexOf(opStack.pop())]();
                                e.left = valStack.pop();
                                valStack.push(e);
                            }
                        }
                        lastVal = true;
                    } else {
                        // if for postfix operators
                        if (lastVal && (tokens[pp].valuestr == "++" || tokens[pp].valuestr == "--")) {
                            e = new opClass[opPrio.indexOf(tokens[pp].valuestr)]();
                            e.left = valStack.pop();
                            e.postfix = true;
                            valStack.push(e);
                            pp++;
                            lastVal = true;
                        } else {
                            parseExpressionArith_rollStack(opStack, valStack, prio);
                            opStack.push(tokens[pp].valuestr);
                            pp++;
                            lastVal = false;
                        }
                    }
                }
            }
            parseExpressionArith_rollStack(opStack, valStack, 1000);

            return valStack.shift();
        }


        private function parseValue():EValue {
            if (tokens[pp] is TokenString) {
                var gs:EValueString = new EValueString();
                gs.value = tokens[pp++].valuestr;

                return parseValueOperators(gs);
            } else if (tokens[pp].valuestr == "true") {
                pp++;
                return new ETrue();
            } else if (tokens[pp].valuestr == "false") {
                pp++;
                return new EFalse();
            } else if (tokens[pp].valuestr == "this") {
                pp++;
                var thisValue:EThis = new EThis();
                return parseValueOperators(thisValue);
            } else if (tokens[pp].valuestr != null) {
                var gv:EValueVar = new EValueVar();
                gv.name = tokens[pp++].valuestr;

                return parseValueOperators(gv);
            } else {
                var gn:EValueNumber = new EValueNumber();
                gn.value = tokens[pp++].valuenum;
                return gn;
            }
        }

        private function parseValueField(left:EValue):EValue {
            var gf:EValueField = new EValueField();
            gf.from = left;
            gf.fieldName = tokens[pp++].valuestr;

            return parseValueOperators(gf);
        }

        private function parseValueIndex(left:EValue):EValue {
            var gi:EValueIndex = new EValueIndex();
            gi.from = left;
            gi.index = parseExpression();
            parseToken("[");
            return parseValueOperators(gi);
        }

        private function parseValueCall(left:EValue):EValue {
            var gc:EValueCall = new EValueCall();
            gc.callee = left;
            parseArgsList(gc.args);

            return parseValueOperators(gc);
        }

        private function parseValueOperators(left:EValue):EValue {
            if (pp < tokens.length) {
                if (tokens[pp].valuestr == ".") {
                    pp++;
                    return parseValueField(left);
                } else if (tokens[pp].valuestr == "[") {
                    pp++;
                    return parseValueIndex(left);
                } else if (tokens[pp].valuestr == "(") {
                    return parseValueCall(left);
                }
            }
            return left;
        }
    }
}
import flash.display.GradientType;
import flash.display.ShaderParameter;
import flash.utils.getQualifiedClassName;

class Environment {
    public var root:Object;
    public var defaultScopeObjects:Vector.<Object> = new Vector.<Object>();
    public var returnFlag:Boolean;
    public var breakFlag:Boolean;
    public var callScopes:Vector.<Object> = new Vector.<Object>();

    public function get scope():Object {
        return callScopes[callScopes.length - 1];
    }

    public function initDefaultScope():void {
        callScopes.length = 0;
        for (var i:int = 0; i < defaultScopeObjects.length; ++i) {
            callScopes.push(defaultScopeObjects[i]);
        }
    }

    public function findPropertyInScope(name:String):Object {
        for (var i:int = callScopes.length -1; i >= 0; --i) {
            if (callScopes[i].hasOwnProperty(name)) {
                return callScopes[i][name];
            }
        }
        return null;
    }

    public function getPropertyScope(name:String):Object {
        for (var i:int = callScopes.length -1; i >= 0; --i) {
            if (callScopes[i].hasOwnProperty(name)) {
                return callScopes[i];
            }
        }
        return null;
    }

    public function setPropertyInScope(name:String, value:Object):void {
        var scope:Object;
        if ((scope = getPropertyScope(name))) {
            scope[name] = value;
        } else {
            for (var i:int = callScopes.length -1; i >= 0; --i) {
                if (getQualifiedClassName(callScopes[i]) == "Object") {
                    callScopes[i][name] = value;
                }
            }
        }
    }

    public function addDefaultScopeObject(scopeObject:Object):void {
        defaultScopeObjects.push(scopeObject);
    }
}

class Token {
    public var valuestr:String;
    public var valuenum:Number;

    public function toString():String {
        return valuestr != null ? valuestr : valuenum.toString();
    }
}

class TokenNumber extends Token {
}

class TokenWord extends Token {
}

class TokenString extends Token {
}

class TokenSymbol extends Token {
}



class Expression {
    public function getValue(env:Environment):Object {
        return null;
    }
}

class EValue extends Expression {
    public function setValue(env:Environment, val:Object):void {
    }
}

class EValueVar extends EValue {
    public var name:String;

    override public function getValue(env:Environment):Object {
        var val:Object;
        if ((val = env.findPropertyInScope(name))!=null) {
            return val;
        } else {
            throw "Variable not found in this scope: '" + name + "'.";
        }
    }

    override public function setValue(env:Environment, val:Object):void {
        env.setPropertyInScope(name, val);
    }
}

class EValueNumber extends EValue {
    public var value:Number;

    override public function getValue(env:Environment):Object {
        return value;
    }
}

class EValueString extends EValue {
    public var value:String;

    override public function getValue(env:Environment):Object {
        return value;
    }
}

class EValueField extends EValue {
    public var from:EValue;
    public var fieldName:String;

    override public function getValue(env:Environment):Object {
        if (from is EThis) {
            return env.findPropertyInScope(fieldName);
        } else {
            var parent:Object = from.getValue(env);
            return parent[fieldName];
        }
    }

    override public function setValue(env:Environment, val:Object):void {
        if (from is EThis) {
            env.setPropertyInScope(fieldName, val);
        } else {
            var parent:Object = from.getValue(env);
            parent[fieldName] = val;
        }
    }
}

class EValueIndex  extends EValue {
    public var from:EValue;
    public var index:Expression;

    override public function getValue(env:Environment):Object {
        if (from is EThis) {
            return env.findPropertyInScope(index.getValue(env).toString());
        } else {
            var parent:Object = from.getValue(env);
            return parent[index.getValue(env)];
        }
    }

    override public function setValue(env:Environment, val:Object):void {
        if (from is EThis) {
            env.setPropertyInScope(index.getValue(env).toString(), val);
        } else {
            var parent:Object = from.getValue(env);
            parent[index.getValue(env)] = val;
        }
    }
}

class EValueCall extends EValue {
    public var callee:EValue;
    public var args:Vector.<Expression> = new Vector.<Expression>();

    override public function getValue(env:Environment):Object {
        var val:Object = callee.getValue(env);
        if (val is Function) {
            var f:Function = val as Function;
            var argsvals:Array = new Array();
            for (var i:int = 0; i < args.length; ++i ) {
                argsvals.push(args[i].getValue(env));
            }
            return f.apply(null, argsvals);
        } else { // if (val is EFunction) {
            return (val as EFunction).apply(env, null, args);
        }
    }
}

class EFunction extends Expression {
    public var args:Vector.<String> = new Vector.<String>();
    public var body:Expression;

    public function apply(env:Environment, thisObj:Object, argsArr:Vector.<Expression>):Object {
        var scope:Object = new Object();
        for (var i:int = 0; i < args.length; ++i) {
            scope[args[i]] = argsArr[i].getValue(env);
        }
        env.callScopes.push(scope);
        var val:Object = body.getValue(env);
        env.callScopes.pop();
        env.returnFlag = false;
        return val;
    }

     override public function getValue(env:Environment):Object {
         return this;
     }
}


class EArith extends Expression {
    public var left:Expression;
    public var right:Expression;
    public var postfix:Boolean;
}

class EAdd extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) + right.getValue(env);
    }
}

class ESub extends EArith {
    override public function getValue(env:Environment):Object {
        return (Number)(left.getValue(env)) - (Number)(right.getValue(env));
    }
}

class EMul extends EArith {
    override public function getValue(env:Environment):Object {
        return (Number)(left.getValue(env)) * (Number)(right.getValue(env));
    }
}

class EDiv extends EArith {
    override public function getValue(env:Environment):Object {
        return (Number)(left.getValue(env)) / (Number)(right.getValue(env));
    }
}

class EMod extends EArith {
    override public function getValue(env:Environment):Object {
        return (Number)(left.getValue(env)) % (Number)(right.getValue(env));
    }
}

class EAnd extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) && right.getValue(env);
    }
}

class EOr extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) || right.getValue(env);
    }
}

class EAssign extends EArith {

    override public function getValue(env:Environment):Object {
        var val:Object = right.getValue(env);
        (left as EValue).setValue(env, val);
        return val;
    }
}

class ENeg extends EArith {
    override public function getValue(env:Environment):Object {
        return -(Number)(left.getValue(env));
    }
}

class EDec extends EArith {
    override public function getValue(env:Environment):Object {
        var val:Number = (Number)(left.getValue(env));
        (left as EValue).setValue(env, val -1);
        return postfix ? val : val - 1;
    }
}

class EInc extends EArith {
    override public function getValue(env:Environment):Object {
        var val:Number = (Number)(left.getValue(env));
        (left as EValue).setValue(env, val + 1);
        return postfix ? val : val + 1;
    }
}

class ENot extends EArith {
    override public function getValue(env:Environment):Object {
        return !left.getValue(env);
    }
}

class ENotEqual extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) != right.getValue(env);
    }
}

class EEqual extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) == right.getValue(env);
    }
}

class EGEqual extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) >= right.getValue(env);
    }
}

class ELEqual  extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) <= right.getValue(env);
    }
}

class EGreater  extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) > right.getValue(env);
    }
}

class ELess extends EArith {
    override public function getValue(env:Environment):Object {
        return left.getValue(env) < right.getValue(env);
    }
}

class ETrue extends EValue {
    override public function getValue(env:Environment):Object {
        return true;
    }
}

class EFalse extends EValue {
    override public function getValue(env:Environment):Object {
        return false;
    }
}

class EThis extends EValue {
}

class EBreak extends Expression {
    override public function getValue(env:Environment):Object {
        env.breakFlag = true;
        return true;
    }
}

class EReturn extends Expression {
    public var e:Expression;

    override public function getValue(env:Environment):Object {
        var val:Object = e.getValue(env);
        env.returnFlag = true;
        return val;
    }
}

class ENew extends Expression {
    public var args:Vector.<Expression> = new Vector.<Expression>();
    public var className:String;

    override public function getValue(env:Environment):Object {
        var c:Class = env.root[className] as Class;
        var av:Array = new Array();
        for (var i:int = 0; i < args.length; ++i) {
            av.push(args[i].getValue(env));
        }
        // that's kind of dirty hack, because as3 doesn't provide way to call ctro with arbitrary number of args
        switch (args.length) {
            case 0: return new c();
            case 1: return new c(av[0]);
            case 2: return new c(av[0], av[1]);
            case 3: return new c(av[0], av[1], av[2]);
            case 4: return new c(av[0], av[1], av[2], av[3]);
            case 5: return new c(av[0], av[1], av[2], av[3], av[4]);
            case 6: return new c(av[0], av[1], av[2], av[3], av[4], av[5]);
            case 7: return new c(av[0], av[1], av[2], av[3], av[4], av[5], av[6]);
            case 8: return new c(av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7]);
            case 9: return new c(av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7], av[8]);
            case 10: return new c(av[0], av[1], av[2], av[3], av[4], av[5], av[6], av[7], av[8], av[9]);
            default: throw "Unsupported constructor argument count!."
        }
    }
}

class EIf extends Expression {
    public var cond:Expression;
    public var ifTrue:Expression;
    public var ifFalse:Expression;

    override public function getValue(env:Environment):Object {
        if (cond.getValue(env)) {
            return ifTrue.getValue(env);
        } else if (ifFalse) {
            return ifFalse.getValue(env);
        } else {
            return true;
        }
    }
}

class EWhile extends Expression {
    public var cond:Expression;
    public var body:Expression;

    override public function getValue(env:Environment):Object {
        var val:Object;
        while (cond.getValue(env)) {
            val = body.getValue(env);
            if (env.breakFlag) {
                env.breakFlag = false;
                break;
            }
        }
        return val;
    }
}

class EFor extends Expression {
    public var init:Expression;
    public var cond:Expression;
    public var increment:Expression;
    public var body:Expression;

    override public function getValue(env:Environment):Object {
        var val:Object;
        init.getValue(env);
        do {
            val = body.getValue(env);
            if (env.breakFlag) {
                env.breakFlag = false;
                break;
            }
            increment.getValue(env);
        } while (cond.getValue(env));
        return val;
    }
}

class ESwitch extends Expression {
    public var value:Expression;
    public var cases:Vector.<SwitchBranch> = new Vector.<SwitchBranch>();
    public var caseDefault:Expression;

    override public function getValue(env:Environment):Object {
        var val:Object = value.getValue(env);
        var ret:Object;
        var match:Boolean = false;
        for (var i:int = 0; i < cases.length; ++i) {
            if (match || val == cases[i].value.getValue(env)) {
                ret = cases[i].action.getValue(env);
                match = true;
            }
            if (env.breakFlag) {
                break;
            }
        }
        if (caseDefault && !env.breakFlag) {
            ret = caseDefault.getValue(env);
        }
        env.breakFlag = false;
        return ret;
    }

}

class SwitchBranch {
    public var value:Expression;
    public var action:Expression;
}

class EVarDecl extends Expression {
    public var varName:String;
    public var typeName:String;
    public var initExp:Expression;

    public var nextEVarDecl:EVarDecl;

    override public function getValue(env:Environment):Object {
		if (initExp) {
            env.setPropertyInScope(varName, initExp.getValue(env));
		} else {
			env.setPropertyInScope(varName, 0 /* TODO: get default type value */);
		}
        return env.findPropertyInScope(varName);
    }
}

class EImport extends Expression {
}

class Statement extends Expression {
    public var expression:Expression;
    public var nextStatement:Statement;

    override public function getValue(env:Environment):Object {
        if (expression) {
            var val:Object = expression.getValue(env);
            if (env.returnFlag || env.breakFlag) {
                return val;
            } else {
                if (nextStatement) {
                    return nextStatement.getValue(env);
                } else {
                    return val;
                }
            }
        } else {
            return 0;
        }
    }
}
