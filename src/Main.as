package
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author mosowski
	 */
	public class Main extends Sprite
	{
		
		public function Main():void
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);

            var s:As3ception = new As3ception(
                ([
                    "import com.adobe.big.lib.*;",

                    "function fib(n) {",
                    "   if (n==0 || n==1) {",
                    "       return 1;",
                    "   } else {",
                    "       return fib(n-1) + fib(n-2);",
                    "   }",
                    "}",

                    "var test = new TestClass(5);",
                    "test.field = test.field + 1;",
                    "var result:int = test.method(2);",

                    "var foo = function(x:int, y:int):typ {",
                    "   var v = x * y;",
                    "   return v*2;",
                    "}",

                    "switch (test.field) {",
                    "   case 1: ",
                    "       test.field = 0;",
                    "       break;",
                    "   case 8: ",
                    "       test.field = test.field * 2;",
                    "       break;",
                    "   case 113:",
                    "       test.field = 1;",
                    "       break;",
                    "}",

                    "var x = foo(1+1+1, 2) - 7;",

                    "x = (x + 2+3*4+5)/4;",
                    "var z = Math.random() * 100;",
                    "var y = 1;",
                    "while (x > 0) {",
                    "   y = y * x;",
                    "   x--;",
                    "}",
                    "finish(fib(test.field));"

                ]).join("\n"));

            s.addGlobal("Math", Math);
            s.addGlobal("TestClass", TestClass);
            s.addGlobal("finish", result);

            s.eval();
		}

        public function result(r:Object):void {
            trace (r);
        }
		
		private function init(e:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
		}
		
	}
	
}


class TestClass {
    public var field:int;

    public function TestClass(f:int):void {
        field = f;
    }

    public function method(param:int):int {
        field += param;
        return param * 2;
    }
}